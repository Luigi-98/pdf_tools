from PyPDF2 import PdfReader, PdfWriter
from tempfile import NamedTemporaryFile
from ._utils import add_padding_blanks, printer_spread_order


def printer_spread(input_file, out_file, start_with_blank, end_with_blank):
    pdf = PdfReader(input_file)

    temp_pdf = NamedTemporaryFile(suffix=".pdf")

    n_pages = len(pdf.pages)

    _, _, w, h = pdf.pages[0].mediabox

    with PdfWriter(temp_pdf.name) as out:
        paddings = add_padding_blanks(n_pages, start_with_blank, end_with_blank)
        for _ in range(paddings[0]):
            out.add_blank_page(w, h)
        out.append_pages_from_reader(pdf)
        for _ in range(paddings[1]):
            out.add_blank_page(w, h)

    pdf = PdfReader(temp_pdf.name)

    n_pages = len(pdf.pages)

    with PdfWriter(out_file) as out:
        for p in printer_spread_order(n_pages):
            out.add_page(pdf.pages[p])

    temp_pdf.close()