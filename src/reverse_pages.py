from PyPDF2 import PdfReader, PdfWriter


def reverse_pages(input_file, out_file):
    pdf = PdfReader(input_file)

    with PdfWriter(out_file) as out:
        for p in pdf.pages[::-1]:
            out.add_page(p)
