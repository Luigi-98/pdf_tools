from typing import Optional, Tuple, Iterable
import os


def add_padding_blanks(n_pages: int, start_with_blank: bool, end_with_blank: bool) -> Tuple[int, int]:
    padding_before = 0
    if start_with_blank:
        padding_before += 1
        n_pages += 1
    padding_after = (4 - (n_pages % 4)) % 4
    if end_with_blank and not padding_after:
        padding_after = 4
    return padding_before, padding_after

def printer_spread_order(n_pages: int) -> Iterable[int]:
    assert not (n_pages % 4), "n_pages must be multiple of 4"
    res = []
    for i in range(1, n_pages // 2 + 1):
        if i%2: # facciata dispari => (n - i, i)
            res.append(n_pages - i)
            res.append(i - 1)
        else: # facciata pari => (i, n - i)
            res.append(i - 1)
            res.append(n_pages - i)
    return res

def out_file(input_file: str, name: str, out_file: Optional[str]=None) -> str:
    if out_file:
        return out_file
    input_name = os.path.splitext(os.path.basename(input_file))
    output_name = input_name[0] + "_" + name + input_name[1]
    return os.path.join(os.path.dirname(input_file), output_name)

