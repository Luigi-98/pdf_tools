#!/usr/bin/env python

from src._utils import out_file
from src.printer_spread import printer_spread
from argparse import ArgumentParser


argparser = ArgumentParser()
argparser.add_argument("--blank-start", action='store_true', help="Starts the document with a blank page instead of the first page.")
argparser.add_argument("--blank-end", action='store_true', help="Ends the document with a blank page instead of the last page.")
argparser.add_argument("INPUT_FILE", help="Path of the input pdf")
argparser.add_argument("--output-file", default=None, help="Path of the output pdf. Defaults to same path as input, but renamed")
args = argparser.parse_args()

start_with_blank = args.blank_start
end_with_blank = args.blank_end

input_file = args.INPUT_FILE

out_file = out_file(input_file, "printer_spread", args.output_file)

printer_spread(input_file, out_file, start_with_blank, end_with_blank)
