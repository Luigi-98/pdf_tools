#!/usr/bin/env python

from src._utils import out_file
from argparse import ArgumentParser
from src.reverse_pages import reverse_pages

argparser = ArgumentParser()
argparser.add_argument("INPUT_FILE", help="Path of the input pdf")
argparser.add_argument("--output-file", default=None, help="Path of the output pdf. Defaults to same path as input, but renamed")
args = argparser.parse_args()

input_file = args.INPUT_FILE

out_file = out_file(input_file, "reversed", args.output_file)

reverse_pages(input_file, out_file)
