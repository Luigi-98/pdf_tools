import pytest
import sys
import os

__whereami__ = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.join(__whereami__, "../"))
from src._utils import add_padding_blanks, printer_spread

@pytest.mark.parametrize("n_pages", list(range(20)))
@pytest.mark.parametrize("start_with_blank", [False, True])
@pytest.mark.parametrize("end_with_blank", [False, True])
def test_padding_blanks(n_pages, start_with_blank, end_with_blank):
    paddings = add_padding_blanks(n_pages, start_with_blank, end_with_blank)
    # check pages are multiples of 4
    assert sum(list(paddings) + [n_pages]) % 4 == 0
    # check if start with blank actually starts with blank
    if start_with_blank:
        assert paddings[0] > 0
    # check if end with blank actually ends with blank
    if end_with_blank:
        assert paddings[1] > 0

@pytest.mark.parametrize("n_pages", list(range(20)))
def test_printer_spread(n_pages):
    if n_pages % 4:
        with pytest.raises(AssertionError):
            p = printer_spread(n_pages)
        return
    p = printer_spread(n_pages)
    if p:  # len(p) is 0, 4, 8, 12, ...
        assert p[0] == n_pages - 1
        assert p[1] == 0
    for l, r in zip(p[::2], p[1::2]):
        assert l + r == n_pages - 1

